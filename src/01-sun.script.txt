The Sun


The sun at the heart of our solar system is a yellow dwarf star, a hot ball of glowing gases. Its gravity holds the solar system together, keeping everything from the biggest planets to the smallest particles of debris in its orbit. Electric currents in the sun generate a magnetic field that is carried out through the solar system by the solar wind -- a stream of electrically charged gas blowing outward from the sun in all directions.

This photo is a view of a solar-flare taken by Skylab 3's Apollo Telescope Mount in 1974.
