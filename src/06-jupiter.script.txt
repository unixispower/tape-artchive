Jupiter


Jupiter is the fifth planet from our sun and the largest planet in the solar system. Jupiter's stripes and swirls are cold, windy clouds of ammonia and water. The atmosphere is mostly hydrogen and helium, and its iconic Great Red Spot is a giant storm bigger than Earth that has raged for hundreds of years.

This processed color image of Jupiter was produced in 1990 by the U.S. Geological Survey from a Voyager image captured in 1979.
