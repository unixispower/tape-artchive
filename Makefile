# Tape Artchive Makefile


# OPTIONS

src_dir := ./src
obj_dir := ./obj

artchive := ./artchive.ogg
artchive_samp_rate := 48000

slide_gap := 1
image_ext := .image.*
audio_ext := .audio.*

script_ext := .script.*
script_voice := cmu_us_awb_cg
script_volume := 3.0

sstv_mode := Robot36
sstv_size := 320x240


# VARS / HELPERS

# sort a list in ascending order
sort_asc = $(shell echo "$(1)" | tr " " "\n" | sort)

# sources and objects
image_srcs := $(call sort_asc, $(wildcard $(src_dir)/*$(image_ext)))
slide_obj_names := $(basename $(basename $(notdir $(image_srcs))))
slide_objs := $(addprefix $(obj_dir)/, $(addsuffix .slide.wav, $(slide_obj_names)))

silence_prefix := $(obj_dir)/silence
gap_obj := $(silence_prefix)-$(slide_gap).wav
empty_audio_obj := $(silence_prefix)-0.wav

slideshow := $(foreach slide_obj,$(slide_objs),$(slide_obj) $(gap_obj))

# RULES

# mark all targets as secondary to prevent them from being deleted
.SECONDARY:

# generate all assets
.PHONY: all
all: $(artchive)

# remove generated objects
.PHONY: clean
clean:
	rm -rf $(artchive) $(obj_dir)

# create object directory
$(obj_dir):
	mkdir -p $@

# join slide wavs into single file
$(artchive): $(slide_objs) $(gap_obj)
	sox $(slideshow) $@

# objects require output directory
$(slide_objs) $(gap_obj): | $(obj_dir)

# join image and audio WAVs as stereo WAV
$(obj_dir)/%.slide.wav: $(obj_dir)/%.image.wav $(obj_dir)/%.audio.wav
	sox --combine merge $^ $@

# encode a resized image to a WAV
$(obj_dir)/%.image.wav: $(obj_dir)/%.image.png
	python -m pysstv \
		--mode $(sstv_mode) \
		--rate $(artchive_samp_rate) \
		 $^ $@

# flatten audio to a single channel
$(obj_dir)/%.audio.wav:: $(src_dir)/%$(audio_ext) | $(obj_dir)
	sox $^ \
		--rate $(artchive_samp_rate) \
		$@ \
		remix -

# generate audio from a script using TTS
$(obj_dir)/%.audio.wav:: $(src_dir)/%$(script_ext) | $(obj_dir)
	text2wave $^ \
		-f $(artchive_samp_rate) \
		-eval "(voice_$(script_voice))" \
		-scale $(script_volume) \
		-o $@

# copy a silent audio channel for missing audio tracks
$(obj_dir)/%.audio.wav: $(empty_audio_obj)
	sox $^ $@ remix -

# resize an image to fit in the SSTV rectangle
$(obj_dir)/%.image.png:: $(src_dir)/%$(image_ext) | $(obj_dir)
	convert $^ \
		-flatten \
		-background black \
		-gravity Center \
		-resize $(sstv_size) \
		-extent $(sstv_size) \
		-unsharp 1.0x1.0+0.5+0.1 \
		-type TrueColor \
		$@

# generate a short file of silence
$(silence_prefix)-%.wav: | $(obj_dir)
	sox --null \
		--rate $(artchive_samp_rate) \
		--channels 2 \
		$@ \
		trim 0.0 $*
