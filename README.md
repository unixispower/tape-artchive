# Tape Artchive
This is a make script that encodes a set of images with audio commentary into
a sound file intended to be recorded on cassette tape. See the
[full project](https://blaines.world/projects/tape-artchive.html) for more
details.


## Customizing Slides
The output of this make script is a sound file (`artchive.ogg`) that contains
all image and audio files in the `src` directory. Image files (`.image.*`) are
processed in order of lexicographically sorted filename. Images are first
resized, then converted to SSTV signals and placed in the object directory
(`obj`). If an audio file (`.audio.*`) exists with the same base name as the
image file, the audio file will be mixed to a single channel and placed in the
object directory. If a script file (`.script.*`) exists with the same base name
as the image file, the script file will be converted to a TTS reading of the
file and placed in the object directory.

After the image object and audio objects are created they are joined into a
stereo file as left and right channels respectively. After all stereo files are
generated they are joined into the output file separated by a small gap of
silence.

Simply replace files in the `src` directory to modify the generated slideshow.
Filesnames must not contain spaces. All image types recognized by ImageMagick
are supported. All audio types recognized by SoX are supported. Settings like
slide gap and TTS voice can be changed by modifying the `Makefile`.


## Generating the Audio File
To generate the audio file you will need the following software:

 - [GNU Make](https://www.gnu.org/software/make/)
 - [PySSTV](https://github.com/dnet/pySSTV)
 - [ImageMagick](https://www.imagemagick.org/)
 - [SoX](http://sox.sourceforge.net/)
 - [Festival](http://festvox.org/festival/)

Run Make in the root of the repository to generate `artchive.ogg` in the root
of the project:
```shell
$ make
```

After you are done you may wish to remove the created files. To remove created
files run:
```shell
$ make clean
```


## Recording a Cassette
To record a cassette you will need:

 - Computer with an audio line-out
 - Cassette tape recorder with line-in
 - Blank cassette tape
 - Audio cable to connect the tape recorder to the computer
 - Software capable of playing WAV files

Use the following steps to record the audio file to a cassette:

 1. Connect the line output of the computer to the line input of the cassette
    recorder using the audio cable.
 2. Load the cassette tape into the recorder.
 3. Load the output audio file into a media player.
 4. Press record on the recorder.
 5. Press play on the media player.
 6. Wait until media player is finished playing file.
 7. Press stop on the recorder.
 8. Press rewind on the recorder.
 9. Wait for cassette to rewind.
 10. Remove cassette from recorder.

Note: You may need to adjust the volume of your computer before making your
recording. Having the volume too low will create a recording with a low SNR
which will cause images to appear distorted. Having the volume too high can
cause signal clipping which will also cause image distortion.


## Content Licensing
The images and parts of the voice scripts included in this repository are taken
from the [NASA Images collection](https://archive.org/details/nasa) at the
Internet Archive.

 - [The New Solar System](https://archive.org/details/PLAN-PIA02973)
 - [SKYLAB III - EXPERIMENTS ATM](https://archive.org/details/S74-23458)
 - [Outgoing Hemisphere](https://archive.org/details/PIA02418)
 - [Venus - Computer Simulated Global View of the Northern Hemisphere](https://archive.org/details/PLAN-PIA00271)
 - [Full Earth](https://archive.org/details/GPN-2000-001138)
 - [Hubble Snaps Mars](https://archive.org/details/SPD-SLRSY-124)
 - [Jupiter](https://archive.org/details/PLAN-PIA00343)
 - [Cassini - The Mission Continues](https://archive.org/details/365640main_PIA11141)
 - [The Colorful Lives of the Outer Planets](https://archive.org/details/SPD-HUBBLE-STScI-2004-05a)
 - [Neptune](https://archive.org/details/SPD-SLRSY-2115)
 - [VARIOUS PLANETS](https://archive.org/details/C-1995-3421)

Descriptions are taken from the
[NASA Solar System Exploration](https://solarsystem.nasa.gov/planets/) pages.
